package com.ba.gas.monkey.resources.customer.app;

import com.ba.gas.monkey.resources.customer.CustomerV1API;
import com.ba.gas.monkey.services.ClusterService;
import com.ba.gas.monkey.services.DistrictService;
import com.ba.gas.monkey.services.ThanaService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@CustomerV1API
@AllArgsConstructor
public class AppGeoDataResource {

    private final DistrictService districtService;
    private final ThanaService thanaService;
    private final ClusterService clusterService;

    @GetMapping("district")
    public ResponseEntity<?> getDistrictList() {
        return new ResponseEntity<>(districtService.getDropdownList(), HttpStatus.OK);
    }

    @GetMapping("thana/{districtId}")
    public ResponseEntity<?> getThanaList(@PathVariable String districtId) {
        return new ResponseEntity<>(thanaService.getDropdownList(districtId), HttpStatus.OK);
    }

    @GetMapping("cluster/{thanaId}")
    public ResponseEntity<?> getClusterList(@PathVariable String thanaId) {
        return new ResponseEntity<>(clusterService.getDropdownList(thanaId), HttpStatus.OK);
    }

}

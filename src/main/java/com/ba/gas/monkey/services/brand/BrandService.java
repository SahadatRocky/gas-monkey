package com.ba.gas.monkey.services.brand;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.base.BaseService;
import com.ba.gas.monkey.constants.AppConstant;
import com.ba.gas.monkey.dtos.DropdownDTO;
import com.ba.gas.monkey.dtos.brand.BrandBean;
import com.ba.gas.monkey.dtos.brand.BrandDetailsBean;
import com.ba.gas.monkey.dtos.brand.BrandImageListBean;
import com.ba.gas.monkey.dtos.brand.BrandListBean;
import com.ba.gas.monkey.models.Brand;
import com.ba.gas.monkey.models.BrandImage;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service("brandService")
public class BrandService extends BaseService<Brand, BrandBean> {

    public BrandService(BaseRepository<Brand> repository, ModelMapper modelMapper) {
        super(repository, modelMapper);
    }


    public Page<BrandListBean> getBrandList(Pageable pageable) {
        Pageable paging = PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), Sort.by("nameEn"));
        Page<BrandBean> page = super.getList(paging);
        List<BrandListBean> list = page.getContent().stream().map(this::getBrandListBean).collect(Collectors.toUnmodifiableList());
        return new PageImpl<>(list, page.getPageable(), page.getTotalElements());
    }


    public BrandDetailsBean getBrandDetailsBean(String id) {
        Brand brand = getById(id);
        BrandDetailsBean bean = getModelMapper().map(brand, BrandDetailsBean.class);
        bean.setBrandImages(brand.getBrandImageList().stream().map(this::getImageListBean).collect(Collectors.toUnmodifiableList()));
        return bean;
    }

    public List<DropdownDTO> dropdownList() {
        List<Brand> list = getRepository().findAll(Sort.by("nameEn")).stream().filter(Brand::getStatus).collect(Collectors.toUnmodifiableList());
        return list.stream().map(this::getDropdownDTO).collect(Collectors.toUnmodifiableList());
    }

    private DropdownDTO getDropdownDTO(Brand brand) {
        DropdownDTO dto = getModelMapper().map(brand, DropdownDTO.class);
        dto.setValue(brand.getNameEn() + " ( " + brand.getNameBn() + " )");
        return dto;
    }

    public BrandBean createBrand(BrandBean brandBean) {
        String shortCode = (brandBean.getNameEn().length() > 3) ? brandBean.getNameEn().replaceAll("\\s+", "").substring(0, 3).toUpperCase() : brandBean.getNameEn().trim();
        brandBean.setShortName(shortCode);
        return super.create(convertForCreate(brandBean));
    }

    public BrandBean updateBrand(String brandId, BrandBean brandBean) {
        String shortCode = (brandBean.getNameEn().length() > 3) ? brandBean.getNameEn().replaceAll("\\s+", "").substring(0, 3).toUpperCase() : brandBean.getNameEn().trim();
        brandBean.setShortName(shortCode);
        Brand brand = getById(brandId);
        BeanUtils.copyProperties(brandBean, brand, AppConstant.IGNORE_PROPERTIES);
        return super.update(brandId, brand);
    }


    private BrandListBean getBrandListBean(BrandBean brand) {
        BrandListBean bean = getModelMapper().map(brand, BrandListBean.class);
        bean.setStatus(brand.getStatus() ? AppConstant.ACTIVE : AppConstant.INACTIVE);
        bean.setImageLink(brand.getBrandImageList().size()>0?brand.getBrandImageList().get(0).getImageLink():"");
        return bean;
    }


    private BrandImageListBean getImageListBean(BrandImage brandImage) {
        BrandImageListBean bean = getModelMapper().map(brandImage, BrandImageListBean.class);
        bean.setStatus(brandImage.getStatus() ? AppConstant.ACTIVE : AppConstant.INACTIVE);
        return bean;
    }
}

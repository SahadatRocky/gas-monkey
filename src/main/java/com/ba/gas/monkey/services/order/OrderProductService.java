package com.ba.gas.monkey.services.order;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.base.BaseService;
import com.ba.gas.monkey.dtos.order.OrderProductBean;
import com.ba.gas.monkey.models.OrderProduct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service("orderProductService")
public class OrderProductService extends BaseService<OrderProduct, OrderProductBean> {
    public OrderProductService(BaseRepository<OrderProduct> repository, ModelMapper modelMapper) {
        super(repository, modelMapper);
    }
}

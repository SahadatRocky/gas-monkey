package com.ba.gas.monkey.repositories;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.models.UserInfo;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface UserInfoRepository extends BaseRepository<UserInfo> {

    @Query("SELECT u FROM UserInfo u WHERE u.email=:email")
    Optional<UserInfo> findByEmailAddress(String email);
}

package com.ba.gas.monkey.dtos.customer;

import com.ba.gas.monkey.models.CustomerType;
import com.ba.gas.monkey.validators.ValidEntityId;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class CustomerInfoBean {
    private String id;
    @NotBlank
    private String customerName;
    @Email
    private String emailAddress;
    @Size(min = 11, max = 16)
    private String phoneNo;
    @ValidEntityId(CustomerType.class)
    private String customerTypeId;
    private String companyName;
    private Boolean status;
    private String districtId;
    private String thanaId;
    private String area;
    private String address;
    @NotNull
    private Double latitude;
    @NotNull
    private Double longitude;
    private String floorNo;
    private Boolean liftAllowed;
    private Integer rewardPoint;
}

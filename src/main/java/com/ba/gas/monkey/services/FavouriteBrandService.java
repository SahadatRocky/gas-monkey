package com.ba.gas.monkey.services;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.base.BaseService;
import com.ba.gas.monkey.constants.AppConstant;
import com.ba.gas.monkey.dtos.FavouriteBrandBean;
import com.ba.gas.monkey.dtos.brand.FavouriteBrandRequestBean;
import com.ba.gas.monkey.models.Brand;
import com.ba.gas.monkey.models.Customer;
import com.ba.gas.monkey.models.FavouriteBrand;
import com.ba.gas.monkey.repositories.FavouriteBrandRepository;
import com.ba.gas.monkey.services.brand.BrandService;
import com.ba.gas.monkey.services.customer.CustomerService;
import com.ba.gas.monkey.services.user.UserInfoService;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.Optional;

@Service("favouriteBrandService")
public class FavouriteBrandService extends BaseService<FavouriteBrand, FavouriteBrandBean> {

    private final BrandService brandService;
    private final CustomerService customerService;
    private final UserInfoService userInfoService;

    public FavouriteBrandService(BaseRepository<FavouriteBrand> repository, ModelMapper modelMapper,
                                 BrandService brandService,
                                 CustomerService customerService,
                                 UserInfoService userInfoService
    ) {
        super(repository, modelMapper);
        this.brandService = brandService;
        this.customerService = customerService;
        this.userInfoService = userInfoService;
    }

    public void favouriteBrand(FavouriteBrandRequestBean bean) {
        Brand brand = brandService.getRepository().getById(bean.getBrandId());
        Customer customer = customerService.getRepository().getById(bean.getCustomerId());
        Optional<FavouriteBrand> optional = ((FavouriteBrandRepository) getRepository()).findByBrandAndCustomer(brand, customer);
        optional.ifPresent(favouriteBrand -> getRepository().delete(favouriteBrand));
        FavouriteBrand favouriteBrand = new FavouriteBrand();
        favouriteBrand.setBrand(brand);
        favouriteBrand.setCustomer(customer);
        favouriteBrand.setStatus(bean.getStatus());
        favouriteBrand.setDateCreated(Instant.now());
        favouriteBrand.setDateModified(Instant.now());
        favouriteBrand.setUpdtId(userInfoService.getByEmailAddress(AppConstant.SUPER_ADMIN_EMAIL).get().getId());
        convertForRead(getRepository().save(favouriteBrand));
    }
}

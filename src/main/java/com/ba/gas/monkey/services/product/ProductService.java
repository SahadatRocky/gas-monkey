package com.ba.gas.monkey.services.product;

import com.ba.gas.monkey.base.BaseEntity;
import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.base.BaseService;
import com.ba.gas.monkey.constants.AppConstant;
import com.ba.gas.monkey.dtos.brand.PopularBrandBean;
import com.ba.gas.monkey.dtos.product.*;
import com.ba.gas.monkey.models.Brand;
import com.ba.gas.monkey.models.Product;
import com.ba.gas.monkey.models.ProductImage;
import com.ba.gas.monkey.repositories.ProductRepository;
import com.ba.gas.monkey.services.brand.BrandService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Service("productService")
public class ProductService extends BaseService<Product, ProductBean> {

    private final BrandService brandService;
    private final ProductSizeService productSizeService;
    private final ProductValveSizeService productValveSizeService;
    private final ProductPriceService productPriceService;
    private final ProductRepository productRepository;

    public ProductService(BaseRepository<Product> repository, ModelMapper modelMapper, BrandService brandService, ProductSizeService productSizeService, ProductValveSizeService productValveSizeService, ProductPriceService productPriceService, ProductRepository productRepository) {
        super(repository, modelMapper);
        this.brandService = brandService;
        this.productSizeService = productSizeService;
        this.productValveSizeService = productValveSizeService;
        this.productPriceService = productPriceService;
        this.productRepository = productRepository;
    }

    public Page<ProductListBean> getProductList(Pageable pageable) {
        Page<Product> page = getRepository().findAll(pageable);
        return new PageImpl<>(page.getContent().stream().map(this::getProductListBean).collect(Collectors.toUnmodifiableList()), pageable, page.getTotalElements());
    }

    public List<PopularBrandBean> getFeatureProductList() {
        List<Product> featureProductList = getRepository().findAll().stream().filter(l -> Objects.nonNull(l.getFeatureProduct()) && l.getFeatureProduct()).collect(Collectors.toUnmodifiableList());
        return featureProductList.stream().map(this::getFeatureProductListBean).collect(Collectors.toList());
    }



    public ProductDetailsBean getProductById(String id) {
        Product product = getById(id);
        ProductDetailsBean bean = getModelMapper().map(product, ProductDetailsBean.class);
        bean.setProductImageList(product.getProductImageList().stream().map(this::getImageListBean).collect(Collectors.toUnmodifiableList()));
        return bean;
    }

    public ProductDetailsBean getProductDetailsFilter(ProductDetailFilterBean productDetailFilterBean) {
        List<Product> productList = productRepository.findByBrandAndProductSizeAndProductValveSize(brandService.getRepository().getById(productDetailFilterBean.getBrandId()), productSizeService.getRepository().getById(productDetailFilterBean.getProductSizeId()), productValveSizeService.getRepository().getById(productDetailFilterBean.getProductValveSizeId()));
        if (productList.isEmpty()) {
            return new ProductDetailsBean();
        } else {
            return getModelMapper().map(productList.get(0), ProductDetailsBean.class);
        }
    }


    public ProductBean createProduct(ProductCreateBean productCreateBean) {
        Product product = getModelMapper().map(productCreateBean, Product.class);
        product.setBrand(brandService.getRepository().getById(productCreateBean.getBrandId()));
        product.setProductSize(productSizeService.getRepository().getById(productCreateBean.getProductSizeId()));
        product.setProductValveSize(productValveSizeService.getRepository().getById(productCreateBean.getProductValveSizeId()));
        return create(product);
    }

    public ProductDetailsBean updateProduct(String id, ProductEditBean productEditBean) {
        Product product = getById(id);
        BeanUtils.copyProperties(productEditBean, product);
        product.setBrand(brandService.getRepository().getById(productEditBean.getBrandId()));
        product.setProductSize(productSizeService.getRepository().getById(productEditBean.getProductSizeId()));
        product.setProductValveSize(productValveSizeService.getRepository().getById(productEditBean.getProductValveSizeId()));
        ProductBean updatedProduct = update(product.getId(), product);
        productPriceService.updateProductPrice(product, productEditBean.getProductPriceBean());
        return getProductById(updatedProduct.getId());
    }

    private ProductListBean getProductListBean(Product product) {
        Double refilPrice = null;
        Double emptyCylinerPrice = null;
        Double packagePrice = null;
        if (Objects.nonNull(product.getProductPrice())) {
            refilPrice = product.getProductPrice().getRefillPrice();
            emptyCylinerPrice = product.getProductPrice().getEmptyCylinderPrice();
            packagePrice = product.getProductPrice().getPackagePrice();
        }
        String productPhoto = null;
        if (!product.getProductImageList().isEmpty()) {
            productPhoto = product.getProductImageList().get(0).getImageLink();
        }

        return ProductListBean.builder()
                .id(product.getId())
                .brandNameEn(product.getBrand().getNameEn())
                .brandNameBn(product.getBrand().getNameBn())
                .companyName(product.getBrand().getCompanyName())
                .productSizeEn(product.getProductSize().getNameEn())
                .productSizeBn(product.getProductSize().getNameBn())
                .valveSizeEn(product.getProductValveSize().getNameEn())
                .valveSizeBn(product.getProductValveSize().getNameBn())
                .refillPrice(refilPrice)
                .emptyCylinderPrice(emptyCylinerPrice)
                .packagePrice(packagePrice)
                .productPhoto(productPhoto)
                .productCode(product.getCode())
                .featureProduct(product.getFeatureProduct()?AppConstant.ACTIVE : AppConstant.INACTIVE)
                .offerProduct(product.getOfferProduct()?AppConstant.ACTIVE : AppConstant.INACTIVE)
                .status(product.getStatus() ? AppConstant.ACTIVE : AppConstant.INACTIVE).build();
    }


    private PopularBrandBean getFeatureProductListBean(Product product) {

        String productPhoto = null;
        if (!product.getProductImageList().isEmpty()) {
            productPhoto = product.getProductImageList().get(0).getImageLink();
        }
        return PopularBrandBean.builder().id(product.getId()).brandNameEn(product.getBrand().getNameEn()).brandNameBn(product.getBrand().getNameBn()).rating("1.0").review("10 reviews").imageUrl(productPhoto).favoriteStatus(false).build();
    }

    private ProductImageListBean getImageListBean(ProductImage productImage) {
        ProductImageListBean bean = getModelMapper().map(productImage, ProductImageListBean.class);
        bean.setStatus(productImage.getStatus() ? AppConstant.ACTIVE : AppConstant.INACTIVE);
        return bean;
    }


    public List<ProductDetailsBean> getProductFilter(ProductFilterBean productFilterBean) {
        List<Product> products = new ArrayList<>();
        if (Objects.nonNull(productFilterBean.getBrandIds()) && !productFilterBean.getBrandIds().isEmpty()) {
            products.addAll(((ProductRepository) getRepository()).findAllByBrandIds(productFilterBean.getBrandIds()));
        }
        if (Objects.nonNull(productFilterBean.getProductSizeIds()) && !productFilterBean.getProductSizeIds().isEmpty()) {
            products.addAll(((ProductRepository) getRepository()).findAllByProductSizeIds(productFilterBean.getProductSizeIds()));
        }
        if (Objects.nonNull(productFilterBean.getProductValveSizeIds()) && !productFilterBean.getProductValveSizeIds().isEmpty()) {
            products.addAll(((ProductRepository) getRepository()).findAllByProductValveSizeIds(productFilterBean.getProductValveSizeIds()));
        }
        List<Product> list = products.stream().filter(distinctByKey(BaseEntity::getId)).collect(Collectors.toUnmodifiableList());
        if (Objects.nonNull(productFilterBean.getProductSizeIds()) && !productFilterBean.getProductSizeIds().isEmpty()) {
            list = list.stream().filter(p -> productFilterBean.getProductSizeIds().contains(p.getProductSize().getId())).collect(Collectors.toUnmodifiableList());
        }
        if (Objects.nonNull(productFilterBean.getProductValveSizeIds()) && !productFilterBean.getProductValveSizeIds().isEmpty()) {
            list = list.stream().filter(p -> productFilterBean.getProductValveSizeIds().contains(p.getProductValveSize().getId())).collect(Collectors.toUnmodifiableList());
        }
        return list.stream().map(p -> getModelMapper().map(p, ProductDetailsBean.class)).collect(Collectors.toUnmodifiableList());
    }

    public static <T> Predicate<T> distinctByKey(Function<? super T, Object> keyExtractor) {
        Map<Object, Boolean> map = new ConcurrentHashMap<>();
        return t -> map.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
    }

    public List<Product> getByBrand(Brand brand) {
        return ((ProductRepository) getRepository()).findAllByBrandIds(Collections.singletonList(brand.getId()));
    }

    public List<PopularBrandBean> getPopularBrandList() {
        List<Brand> brands = brandService.getRepository().findAll().stream().filter(Brand::getStatus).collect(Collectors.toUnmodifiableList());
        return brands.stream().map(this::getPopularBrandListBean).collect(Collectors.toList());
    }

    private PopularBrandBean getPopularBrandListBean(Brand brand) {
        List<Product> products = getByBrand(brand);
        String brandImage = (!brand.getBrandImageList().isEmpty()) ? brand.getBrandImageList().get(0).getImageLink() : null;
        return PopularBrandBean.builder().id(brand.getId()).brandNameEn(brand.getNameEn())
                .brandNameBn(brand.getNameBn()).rating("1.0").review("10 reviews")
                .imageUrl(brandImage).favoriteStatus(false).product(!products.isEmpty() ? products.get(0) : null).build();
    }

//    public List<PopularBrandBean> getOfferedProductList() {
//        List<Product> offeredProductList = getRepository().findAll().stream().filter(l -> Objects.nonNull(l.getOfferProduct()) && l.getOfferProduct()).collect(Collectors.toUnmodifiableList());
//        return offeredProductList.stream().map(this::getFeatureProductListBean).collect(Collectors.toList());
//    }

    public List<PopularBrandBean> getOfferedProductList() {
        List<Brand> brands = brandService.getRepository().findAll().stream().filter(Brand::getStatus).filter(Brand::getDiscountEnabled).collect(Collectors.toUnmodifiableList());
        return brands.stream().map(this::getOfferBrandListBean).collect(Collectors.toList());
    }

    private PopularBrandBean getOfferBrandListBean(Brand brand) {
        List<Product> products = getByBrand(brand);
        String brandImage = (!brand.getBrandImageList().isEmpty()) ? brand.getBrandImageList().get(0).getImageLink() : null;
        return PopularBrandBean.builder().id(brand.getId()).brandNameEn(brand.getNameEn())
                .brandNameBn(brand.getNameBn()).rating("1.0").review("10 reviews")
                .imageUrl(brandImage).favoriteStatus(false).product(!products.isEmpty() ? products.get(0) : null).build();
    }

    public List<PopularBrandBean> getAllBrandList() {
        List<Brand> brands = brandService.getRepository().findAll().stream().filter(Brand::getStatus).collect(Collectors.toUnmodifiableList());
        return brands.stream().map(this::getPopularBrandListBean).collect(Collectors.toList());
    }
}

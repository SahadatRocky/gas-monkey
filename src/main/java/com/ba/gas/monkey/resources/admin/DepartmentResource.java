package com.ba.gas.monkey.resources.admin;

import com.ba.gas.monkey.services.brand.BrandService;
import com.ba.gas.monkey.services.department.DepartmentService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;

@AdminV1API
@AllArgsConstructor
public class DepartmentResource {

    private final DepartmentService service;

    @GetMapping("department/dropdown-list")
    public ResponseEntity<?> getDropdownList() {
        return new ResponseEntity<>(service.getDropdownList(), HttpStatus.OK);
    }

}

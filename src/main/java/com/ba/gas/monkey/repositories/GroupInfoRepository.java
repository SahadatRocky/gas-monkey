package com.ba.gas.monkey.repositories;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.models.GroupInfo;

public interface GroupInfoRepository extends BaseRepository<GroupInfo> {
}

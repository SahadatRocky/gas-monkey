package com.ba.gas.monkey.services.order;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.base.BaseService;
import com.ba.gas.monkey.dtos.order.AdminOrderDetailsBean;
import com.ba.gas.monkey.dtos.order.CreateOrderRequestBean;
import com.ba.gas.monkey.dtos.order.OrderBean;
import com.ba.gas.monkey.dtos.order.OrderInfoListBean;
import com.ba.gas.monkey.models.Cart;
import com.ba.gas.monkey.models.CartDetail;
import com.ba.gas.monkey.models.OrderInfo;
import com.ba.gas.monkey.models.OrderProduct;
import com.ba.gas.monkey.repositories.OrderRepository;
import com.ba.gas.monkey.services.ClusterService;
import com.ba.gas.monkey.services.DistrictService;
import com.ba.gas.monkey.services.ThanaService;
import com.ba.gas.monkey.services.cart.CartDetailService;
import com.ba.gas.monkey.services.cart.CartService;
import com.ba.gas.monkey.services.customer.CustomerService;
import com.ba.gas.monkey.services.user.UserInfoService;
import com.ba.gas.monkey.utility.Util;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service("orderService")
public class OrderService extends BaseService<OrderInfo, OrderBean> {

    private final CartService cartService;
    private final CartDetailService cartDetailService;
    private final OrderRepository orderRepository;
    private final OrderProductService orderProductService;
    private final DistrictService districtService;
    private final ThanaService thanaService;
    private final ClusterService clusterService;
    private final CustomerService customerService;
    private final UserInfoService userInfoService;

    public OrderService(BaseRepository<OrderInfo> repository, ModelMapper modelMapper,
                        CartService cartService, CartDetailService cartDetailService,
                        OrderRepository orderRepository, OrderProductService orderProductService,
                        DistrictService districtService,
                        ThanaService thanaService,
                        ClusterService clusterService, CustomerService customerService,
                        UserInfoService userInfoService) {
        super(repository, modelMapper);
        this.cartService = cartService;
        this.cartDetailService = cartDetailService;
        this.orderRepository = orderRepository;
        this.orderProductService = orderProductService;
        this.districtService = districtService;
        this.thanaService = thanaService;
        this.clusterService = clusterService;
        this.customerService = customerService;
        this.userInfoService = userInfoService;
    }

    public OrderBean createOrder(CreateOrderRequestBean bean) {
        Cart cart = cartService.getRepository().getById(bean.getCartId());
        OrderInfo orderInfo = getModelMapper().map(bean, OrderInfo.class);
        orderInfo.setDistrict(districtService.getRepository().getById(bean.getDistrictId()));
        orderInfo.setThana(thanaService.getRepository().getById(bean.getThanaId()));
        orderInfo.setCluster(clusterService.getRepository().getById(bean.getClusterId()));
        orderInfo.setCustomer(customerService.getRepository().getById(bean.getCustomerId()));
        orderInfo.setDateCreated(Instant.now());
        orderInfo.setDateModified(Instant.now());
        orderInfo.setOrderNumber(Util.getNext());
        String updtId = userInfoService.getByEmailAddress("sadmin@batworld.com").get().getId();
        orderInfo.setUpdtId(updtId);
        orderInfo.setStatus(Boolean.TRUE);
        OrderInfo createdOrderInfo = getRepository().save(orderInfo);
        cart.getCartDetailList().forEach(cartDetail -> {
            OrderProduct orderProduct = getOrderProduct(cartDetail, createdOrderInfo);
            orderProduct.setUpdtId(updtId);
            orderProductService.getRepository().save(orderProduct);
        });
        cartDetailService.getRepository().deleteAll(cart.getCartDetailList());
        return getModelMapper().map(orderRepository.getById(createdOrderInfo.getId()),OrderBean.class);
    }

    public List<OrderBean> getOrderList(String id) {
        List<OrderInfo> list = orderRepository.findByCustomerId(id).get();
        List<OrderBean> listBean = new ArrayList<>();
        list.forEach(orderInfo -> {
            listBean.add(getModelMapper().map(orderInfo, OrderBean.class));
        });
        return listBean;
    }

    public Page<OrderInfoListBean> getOrderList(Pageable pageable) {
        Page<OrderInfo> page = getRepository().findAll(pageable);
        return new PageImpl<>(page.getContent().stream().map(this::getOrderInfoListBean).collect(Collectors.toUnmodifiableList()), page.getPageable(),page.getTotalElements());
    }

    private OrderInfoListBean getOrderInfoListBean(OrderInfo orderInfo) {
        return OrderInfoListBean.builder()
                .id(orderInfo.getId())
                .orderNumber(orderInfo.getOrderNumber())
                .customerName(orderInfo.getCustomer().getCustomerName())
                .partnerName("N/A")
                .dealerName("N/A")
                .customerPhoneNo(orderInfo.getCustomer().getPhoneNo())
                .productPhoto("N/A")
                .brandName("N/A")
                .orderQuantity(1)
                .productSize("N/A")
                .price(orderInfo.getTotal())
                .valveSize("N/A")
                .orderDate(orderInfo.getDateCreated())
                .orderStatus(orderInfo.getOrderStatus())
                .build();
    }

    public AdminOrderDetailsBean getOrder(String id) {
        return getModelMapper().map(orderRepository.findById(id).get(), AdminOrderDetailsBean.class);
    }

    private OrderProduct getOrderProduct(CartDetail cartDetail, OrderInfo orderInfo) {
        double purchasePrice = cartDetail.getReturnProduct() != null?cartDetail.getBuyProduct().getProductPrice().getRefillPrice():cartDetail.getBuyProduct().getProductPrice().getPackagePrice();

        double exchangeAmount = 0.0;
        if(cartDetail.getReturnProduct() != null && (cartDetail.getBuyProduct().getProductPrice().getEmptyCylinderPrice() > cartDetail.getReturnProduct().getProductPrice().getEmptyCylinderPrice())){
            exchangeAmount = cartDetail.getBuyProduct().getProductPrice().getEmptyCylinderPrice() - cartDetail.getReturnProduct().getProductPrice().getEmptyCylinderPrice();
        }else {
            exchangeAmount = 0;
        }

        return OrderProduct.builder()
                .orderInfo(orderInfo)
                .buyProduct(cartDetail.getBuyProduct())
                .returnProduct(cartDetail.getReturnProduct())
                .quantity(cartDetail.getQuantity())
                .refillPrice(cartDetail.getBuyProduct().getProductPrice().getRefillPrice())
                .emptyCylinderPrice(cartDetail.getBuyProduct().getProductPrice().getEmptyCylinderPrice())
                .productPurchasePrice(purchasePrice)
                .returnEmptyCylinderPrice(cartDetail.getReturnProduct() != null?cartDetail.getReturnProduct().getProductPrice().getEmptyCylinderPrice():0)
                .exchangeAmount(exchangeAmount)
                .build();
    }


}

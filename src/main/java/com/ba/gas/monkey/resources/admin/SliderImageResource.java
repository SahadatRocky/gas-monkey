package com.ba.gas.monkey.resources.admin;

import com.ba.gas.monkey.dtos.sliderImage.SliderImageBean;
import com.ba.gas.monkey.services.SliderImageService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@AdminV1API
@AllArgsConstructor
public class SliderImageResource {

    private final SliderImageService service;

    @GetMapping("slider-image")
    public ResponseEntity<?> getList(Pageable pageable) {
        return new ResponseEntity<>(service.getSliderImages(pageable), HttpStatus.OK);
    }

    @PostMapping("slider-image")
    public ResponseEntity<?> create(@RequestBody SliderImageBean bean) {
        return new ResponseEntity<>(service.createSlider(bean), HttpStatus.OK);
    }

    @PutMapping("slider-image/{id}")
    public ResponseEntity<?> update(@PathVariable String id, @RequestBody SliderImageBean bean) {
        return new ResponseEntity<>(service.updateSlider(id,bean), HttpStatus.OK);
    }

    @DeleteMapping("slider-image/{id}")
    public ResponseEntity<?> deleteSliderImage(@PathVariable("id") String id) {
        return new ResponseEntity<>(service.deleteByOid(id), HttpStatus.OK);
    }
}

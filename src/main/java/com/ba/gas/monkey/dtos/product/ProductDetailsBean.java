package com.ba.gas.monkey.dtos.product;

import com.ba.gas.monkey.dtos.brand.BrandBean;
import com.ba.gas.monkey.dtos.IRequestBodyDTO;
import lombok.Data;

import java.util.List;

@Data
public class ProductDetailsBean implements IRequestBodyDTO {
    private String id;
    private String nameEn;
    private String nameBn;
    private String descriptionEn;
    private String descriptionBn;
    private String code;
    private BrandBean brand;
    private ProductSizeBean productSize;
    private ProductValveSizeBean productValveSize;
    private ProductPriceBean productPrice;
    private Boolean status;
    private Boolean featureProduct;
    private Boolean offerProduct;
    private List<ProductImageListBean> productImageList;
}

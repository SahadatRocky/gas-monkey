package com.ba.gas.monkey.dtos.brand;

import com.ba.gas.monkey.dtos.IRequestBodyDTO;
import com.ba.gas.monkey.models.BrandImage;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.List;

@Data
public class BrandBean implements IRequestBodyDTO {
    private String id;
    @NotBlank(message = "name-en can't be empty")
    private String nameEn;
    @NotBlank(message = "name-bn can't be empty")
    private String nameBn;
    @NotNull
    private Boolean status;
    @NotBlank(message = "company-name can't be empty")
    private String companyName;
    private String shortName;
    private Boolean discountEnabled;
    private String discountType;
    private Double discountValue;
    private LocalDate discountStartDate;
    private LocalDate discountEndDate;
    private List<BrandImage> brandImageList;
}

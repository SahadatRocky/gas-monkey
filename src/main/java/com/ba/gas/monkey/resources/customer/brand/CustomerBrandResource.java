package com.ba.gas.monkey.resources.customer.brand;

import com.ba.gas.monkey.resources.customer.CustomerV1API;
import com.ba.gas.monkey.services.brand.BrandService;
import com.ba.gas.monkey.services.product.ProductService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;

@CustomerV1API
@AllArgsConstructor
public class CustomerBrandResource {

    private final BrandService brandService;
    private final ProductService productService;

    @GetMapping("popular-brand")
    public ResponseEntity<?> getProductList() {
        return new ResponseEntity<>(productService.getPopularBrandList(), HttpStatus.OK);
    }

    @GetMapping("offer-brand")
    public ResponseEntity<?> getOfferProductList() {
        return new ResponseEntity<>(productService.getOfferedProductList(), HttpStatus.OK);
    }

    @GetMapping("all-brand")
    public ResponseEntity<?> getAllProductList() {
        return new ResponseEntity<>(productService.getAllBrandList(), HttpStatus.OK);
    }

    @GetMapping("brand/dropdown-list")
    public ResponseEntity<?> dropdownList() {
        return new ResponseEntity<>(brandService.dropdownList(), HttpStatus.OK);
    }
}

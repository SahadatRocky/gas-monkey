package com.ba.gas.monkey.dtos;

import lombok.Data;

@Data
public class DropdownDTO {
    private String id;
    private String value;
//    private String valueEn;
//    private String valueBn;
}

package com.ba.gas.monkey.services.customer;

import com.ba.gas.monkey.dtos.customer.CustomerBean;
import com.ba.gas.monkey.dtos.customer.CustomerInfoBean;
import com.ba.gas.monkey.dtos.customer.CustomerRegistrationBean;
import com.ba.gas.monkey.exception.ServiceExceptionHolder;
import com.ba.gas.monkey.services.user.UserInfoService;
import lombok.AllArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@AllArgsConstructor
@Service("customerRegistrationService")
public class CustomerRegistrationService {

    private final PasswordEncoder passwordEncoder;
    private final CustomerService customerService;
    private final UserInfoService userInfoService;

    public CustomerInfoBean createCustomerRegistration(CustomerRegistrationBean bean) {
        String password = bean.getPassword();
        String confirmPassword = bean.getConfirmPassword();
        if (!password.equals(confirmPassword)) {
            throw new ServiceExceptionHolder.CustomException(ServiceExceptionHolder.PASSWORD_MISMATCH_EXCEPTION_CODE, "please check password");
        }
        Optional<CustomerBean> optionalCustomer = customerService.getByPhoneNo(bean.getPhoneNo());
        if (optionalCustomer.isPresent()) {
            throw new ServiceExceptionHolder.CustomException(ServiceExceptionHolder.PASSWORD_MISMATCH_EXCEPTION_CODE, bean.getPhoneNo() + " : phone is already in use");
        }
        bean.setPassword(passwordEncoder.encode(password));
        bean.setUpdtId(userInfoService.getByEmailAddress("sadmin@batworld.com").get().getId());
        return customerService.registration(bean);
    }


}

package com.ba.gas.monkey.resources.admin;

import com.ba.gas.monkey.dtos.ConfigurationDTO;
import com.ba.gas.monkey.services.ConfigurationService;
import lombok.AllArgsConstructor;


import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;

@AdminV1API
@AllArgsConstructor
public class ConfigurationResource {

    private final ConfigurationService configurationService;

    @GetMapping("config-data")
    public ResponseEntity<?> getConfigurationData() {
        return new ResponseEntity<>(configurationService.getConfigurationData(), HttpStatus.OK);
    }

    @PostMapping("config-data")
    public ResponseEntity<?> getUpdateConfigurationData(@RequestBody @Valid ConfigurationDTO dto) {
        return new ResponseEntity<>(configurationService.getUpdateConfigurationData(dto), HttpStatus.OK);
    }
}

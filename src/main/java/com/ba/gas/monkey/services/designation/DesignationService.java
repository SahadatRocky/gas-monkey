package com.ba.gas.monkey.services.designation;


import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.base.BaseService;
import com.ba.gas.monkey.dtos.DropdownDTO;
import com.ba.gas.monkey.dtos.designation.DesignationBean;
import com.ba.gas.monkey.models.Designation;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service("designationService")
public class DesignationService extends BaseService<Designation, DesignationBean> {

    public DesignationService(BaseRepository<Designation> repository, ModelMapper modelMapper) {
        super(repository, modelMapper);
    }

    public List<DropdownDTO> getDropdownList() {
        List<Designation> list = getRepository().findAll(Sort.by("name"));
        return list.stream().map(this::getDropdownDto).collect(Collectors.toUnmodifiableList());
    }

    private DropdownDTO getDropdownDto(Designation designation) {
        DropdownDTO dropdownDTO = getModelMapper().map(designation, DropdownDTO.class);
        dropdownDTO.setValue(designation.getName());
        return dropdownDTO;
    }
}

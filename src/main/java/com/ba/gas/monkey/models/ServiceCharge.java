package com.ba.gas.monkey.models;

import com.ba.gas.monkey.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "service_charge")
@EqualsAndHashCode(callSuper = true)
public class ServiceCharge extends BaseEntity {

    @Column(name = "SERVICE_TYPE")
    private String serviceName;
    @Column(name = "SERVICE_VALUE")
    private Double serviceValue;
    @Column(name = "STATUS", columnDefinition = "BIT default 0", length = 1)
    private Boolean status;

}

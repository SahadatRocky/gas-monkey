package com.ba.gas.monkey.services.user;

import com.ba.gas.monkey.constants.AppConstant;
import com.ba.gas.monkey.dtos.group.GroupInfoBean;
import com.ba.gas.monkey.dtos.user.UserInfoBean;
import com.ba.gas.monkey.dtos.user.UserInfoEditBean;
import com.ba.gas.monkey.dtos.user.UserListBean;
import com.ba.gas.monkey.exception.ServiceExceptionHolder;
import com.ba.gas.monkey.models.GroupInfo;
import com.ba.gas.monkey.models.UserInfo;
import com.ba.gas.monkey.repositories.GroupInfoRepository;
import com.ba.gas.monkey.repositories.UserInfoRepository;
import com.ba.gas.monkey.services.department.DepartmentService;
import com.ba.gas.monkey.services.designation.DesignationService;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

@AllArgsConstructor
@Service("userInfoService")
class UserInfoServiceImpl implements UserInfoService {

    private final UserInfoRepository repository;
    private final ModelMapper modelMapper;
    private final GroupInfoRepository groupInfoRepository;
    private final DepartmentService departmentService;
    private final DesignationService designationService;

    @Override
    public Page<UserListBean> getList(Pageable pageable) {
        Page<UserInfo> page = repository.findAll(PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), Sort.by(Sort.Direction.DESC, "dateCreated")));
        List<GroupInfo> groupInfos = groupInfoRepository.findAll();
        List<String> ids = groupInfos.stream().filter(g -> g.getGroupType().equals("SUPER_ADMIN")).collect(Collectors.toUnmodifiableList()).stream().map(GroupInfo::getId).collect(Collectors.toUnmodifiableList());
        List<UserInfo> list = page.getContent().stream().filter(p -> !ids.contains(p.getGroupInfo().getId())).collect(Collectors.toUnmodifiableList());
        return new PageImpl<>(list.stream().map(this::getUserListBean).collect(Collectors.toList()), page.getPageable(), page.getTotalElements());
    }

    @Override
    public Optional<UserInfoBean> getByEmailAddress(String emailAddress) {
        Optional<UserInfo> optionalUserInfo = repository.findByEmailAddress(emailAddress);
        if (optionalUserInfo.isEmpty()) {
            return Optional.empty();
        }
        UserInfoBean userInfoBean = getDto(optionalUserInfo.get());
        List<String> authorities = Collections.singletonList(userInfoBean.getGroupInfoBean().getGroupType());
        userInfoBean.setAuthorities(authorities);
        return Optional.of(userInfoBean);
    }

    @Override
    public UserInfoBean getByUserId(String userId) {
        UserInfo info = repository.findById(userId).orElseThrow(() -> new ServiceExceptionHolder.IdNotFoundInDBException("No user found by id [" + userId + "]"));
        return getDto(info);
    }

    @Override
    public Optional<UserInfoBean> updateUser(String id, UserInfoEditBean userInfoBean) {
        UserInfo userInfo = repository.findById(id).orElseThrow(() -> new ServiceExceptionHolder.IdNotFoundInDBException("No user found by id [" + id + "]"));
        BeanUtils.copyProperties(userInfoBean, userInfo, "id", "dateCreated", "password");
        userInfo.setGroupInfo(groupInfoRepository.getById(userInfoBean.getGroupId()));
        userInfo.setDepartment(departmentService.getRepository().getById(userInfoBean.getDepartmentId()));
        userInfo.setDesignation(designationService.getRepository().getById(userInfoBean.getDesignationId()));
        UserInfo updatedUser = repository.save(userInfo);
        return getByEmailAddress(updatedUser.getEmail());
    }

    @Override
    public Optional<UserInfo> getById(String userId) {
        return repository.findById(userId);
    }

    @Override
    public UserInfoBean getUserDetails(UserInfo userInfo) {
        UserInfo info = repository.getById(userInfo.getId());
        return getDto(info);
    }

    private UserListBean getUserListBean(UserInfo userInfo) {
        UserListBean bean = modelMapper.map(userInfo, UserListBean.class);
        bean.setStatus(userInfo.getStatus() ? AppConstant.ACTIVE : AppConstant.INACTIVE);
        bean.setUserType(userInfo.getGroupInfo().getGroupName());
        bean.setDepartment(userInfo.getDepartment().getName());
        bean.setDesignation(userInfo.getDesignation().getName());
        return bean;
    }

    private UserInfoBean getDto(UserInfo userInfo) {
        UserInfoBean bean = modelMapper.map(userInfo, UserInfoBean.class);
        bean.setGroupInfoBean(modelMapper.map(userInfo.getGroupInfo(), GroupInfoBean.class));
        List<String> roles = new ArrayList<>();
        roles.add(bean.getGroupInfoBean().getGroupType());
        bean.setAuthorities(roles);
        return bean;
    }

    @Override
    public void update(UserInfo userInfo) {
        userInfo.setDateModified(Instant.now());
        repository.save(userInfo);
    }

    private UserInfo getEntity(UserInfoBean userInfoBean) {
        return modelMapper.map(userInfoBean, UserInfo.class);
    }


}

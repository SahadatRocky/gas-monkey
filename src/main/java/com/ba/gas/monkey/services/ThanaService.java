package com.ba.gas.monkey.services;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.base.BaseService;
import com.ba.gas.monkey.dtos.DropdownDTO;
import com.ba.gas.monkey.dtos.ThanaBean;
import com.ba.gas.monkey.exception.ServiceExceptionHolder;
import com.ba.gas.monkey.models.District;
import com.ba.gas.monkey.models.Thana;
import com.ba.gas.monkey.repositories.ThanaRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service("thanaService")
public class ThanaService extends BaseService<Thana, ThanaBean> {

    private final DistrictService districtService;

    public ThanaService(BaseRepository<Thana> repository, ModelMapper modelMapper, DistrictService districtService) {
        super(repository, modelMapper);
        this.districtService = districtService;
    }

    public List<DropdownDTO> getDropdownList(String districtId) {
        District district = districtService.getRepository().findById(districtId).orElseThrow(() -> new ServiceExceptionHolder.IdNotFoundInDBException("No district found by id :" + districtId));
        List<Thana> thanas = ((ThanaRepository) getRepository()).findByDistrict(district);
        return thanas.stream().map(this::getDropdownDTO).collect(Collectors.toUnmodifiableList());
    }

    private DropdownDTO getDropdownDTO(Thana thana) {
        DropdownDTO dto = getModelMapper().map(thana, DropdownDTO.class);
        dto.setValue(thana.getName());
        return dto;
    }
}

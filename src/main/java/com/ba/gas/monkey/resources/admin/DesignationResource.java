package com.ba.gas.monkey.resources.admin;


import com.ba.gas.monkey.services.department.DepartmentService;
import com.ba.gas.monkey.services.designation.DesignationService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;

@AdminV1API
@AllArgsConstructor
public class DesignationResource {

    private final DesignationService service;

    @GetMapping("designation/dropdown-list")
    public ResponseEntity<?> getDropdownList() {
        return new ResponseEntity<>(service.getDropdownList(), HttpStatus.OK);
    }
}

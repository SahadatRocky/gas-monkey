package com.ba.gas.monkey.dtos.designation;

import com.ba.gas.monkey.dtos.IRequestBodyDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DesignationBean implements IRequestBodyDTO {
    private String id;
    private String name;
    private Boolean status;
}

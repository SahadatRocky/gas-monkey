package com.ba.gas.monkey.models;

import com.ba.gas.monkey.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

@Data
@Entity
@Table(name = "thana")
@EqualsAndHashCode(callSuper = true)
public class Thana extends BaseEntity {

    @Column(name = "NAME")
    private String name;
    @Column(name = "STATUS", columnDefinition = "BIT default 0", length = 1)
    private Boolean status;
    @OneToOne
    @JoinColumn(name = "DISTRICT_ID", referencedColumnName = "ID")
    private District district;
}

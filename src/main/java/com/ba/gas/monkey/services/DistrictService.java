package com.ba.gas.monkey.services;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.base.BaseService;
import com.ba.gas.monkey.dtos.DistrictBean;
import com.ba.gas.monkey.dtos.DropdownDTO;
import com.ba.gas.monkey.dtos.ThanaBean;
import com.ba.gas.monkey.models.District;
import com.ba.gas.monkey.models.Thana;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service("districtService")
public class DistrictService extends BaseService<District, DistrictBean> {

    public DistrictService(BaseRepository<District> repository, ModelMapper modelMapper) {
        super(repository, modelMapper);
    }

    public List<DropdownDTO> getDropdownList() {
        return getRepository().findAll().stream().map(this::getDropdownDTO).collect(Collectors.toUnmodifiableList());
    }

    private DropdownDTO getDropdownDTO(District district) {
        DropdownDTO dto = getModelMapper().map(district, DropdownDTO.class);
        dto.setValue(district.getName());
        return dto;
    }
}

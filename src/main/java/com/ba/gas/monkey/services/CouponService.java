package com.ba.gas.monkey.services;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.base.BaseService;
import com.ba.gas.monkey.constants.AppConstant;
import com.ba.gas.monkey.dtos.CouponBean;
import com.ba.gas.monkey.exception.ServiceExceptionHolder;
import com.ba.gas.monkey.models.Coupon;
import com.ba.gas.monkey.repositories.CouponRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

@Service("couponService")
public class CouponService extends BaseService<Coupon, CouponBean> {

    public CouponService(BaseRepository<Coupon> repository, ModelMapper modelMapper) {
        super(repository, modelMapper);
    }

    public CouponBean createCoupon(CouponBean couponBean) {
        return super.create(convertForCreate(couponBean));
    }

    public CouponBean updateCoupon(String id, CouponBean bean) {
        Coupon coupon = getRepository().findById(id).orElseThrow(() -> new ServiceExceptionHolder.IdNotFoundInDBException("No coupon found by id:" + id));
        BeanUtils.copyProperties(bean, coupon, AppConstant.IGNORE_PROPERTIES);
        return update(id, coupon);
    }

    public String deleteByOid(String id) {
        getRepository().deleteById(id);
        return "delete successful";
    }

    public CouponBean getCouponDetailsBean(String code) {
        Coupon coupon = ((CouponRepository)getRepository()).findByCouponCode(code).orElseThrow(() -> new ServiceExceptionHolder.IdNotFoundInDBException("No coupon found by code:" + code));
        return getModelMapper().map(coupon, CouponBean.class);
    }
}

package com.ba.gas.monkey.models;

import com.ba.gas.monkey.base.BaseEntity;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.List;


@Data
@Entity
@Table(name = "cart")
@EqualsAndHashCode(callSuper = true)
public class Cart extends BaseEntity {
    @JsonManagedReference
    @OneToOne
    @JoinColumn(name = "CUSTOMER_ID", referencedColumnName = "ID")
    private Customer customer;
    @Column(name = "STATUS", columnDefinition = "BIT default 0", length = 1)
    private Boolean status;
    @JsonManagedReference
    @OneToMany(mappedBy = "cart")
    private List<CartDetail> cartDetailList;
}

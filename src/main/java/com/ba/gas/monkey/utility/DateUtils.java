package com.ba.gas.monkey.utility;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

public class DateUtils {

    public static Date getToday() {
        return new Date();
    }

    public static boolean isDateInBetweenIncludingEndPoints(final LocalDate min, final LocalDate max, final LocalDateTime date){
        boolean value =  (date.isAfter(min.atStartOfDay()) || date.isBefore(max.plusDays(1).atStartOfDay()));
        return value;
    }
}

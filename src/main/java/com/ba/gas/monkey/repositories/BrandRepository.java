package com.ba.gas.monkey.repositories;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.models.Brand;

public interface BrandRepository extends BaseRepository<Brand> {
}

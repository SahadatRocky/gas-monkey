package com.ba.gas.monkey.models;

import com.ba.gas.monkey.base.BaseEntity;
import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.*;

import javax.persistence.*;


@Data
@Builder
@Entity
@AllArgsConstructor
@Table(name = "order_product")
@EqualsAndHashCode(callSuper = true)
public class OrderProduct extends BaseEntity {
    public OrderProduct() {
    }

    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "ORDER_ID", referencedColumnName = "ID")
    private OrderInfo orderInfo;
    @OneToOne
    @JoinColumn(name = "BUY_PRODUCT_ID", referencedColumnName = "ID")
    private Product buyProduct;
    @OneToOne
    @JoinColumn(name = "RETURN_PRODUCT_ID", referencedColumnName = "ID")
    private Product returnProduct;
    @Column(name = "QUANTITY")
    private Integer quantity;
    @Column(name = "REFILL_PRICE")
    private Double refillPrice;
    @Column(name = "PRODUCT_PURCHASE_PRICE")
    private Double productPurchasePrice;
    @Column(name = "EMPTY_CYLINDER_PRICE")
    private Double emptyCylinderPrice;
    @Column(name = "RETURN_EMPTY_CYLINDER_PRICE")
    private Double returnEmptyCylinderPrice;
    @Column(name = "EXCHANGE_AMOUNT")
    private Double exchangeAmount;
    @Column(name = "PACKAGE_PRICE")
    private Double packagePrice;
}

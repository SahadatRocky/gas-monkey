package com.ba.gas.monkey.models;

import com.ba.gas.monkey.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;


@Data
@Entity
@Table(name = "user_info")
@EqualsAndHashCode(callSuper = true)
public class UserInfo extends BaseEntity {

    @Column(name = "EMAIL")
    private String email;
    @Column(name = "MOBILE")
    private String mobile;
    @Column(name = "USER_FULL_NAME")
    private String userFullName;
    @OneToOne
    @JoinColumn(name = "DEPARTMENT_ID", referencedColumnName = "ID")
    private Department department;
    @OneToOne
    @JoinColumn(name = "DESIGNATION_ID", referencedColumnName = "ID")
    private Designation designation;
    @Column(name = "PASSWORD")
    private String password;
    @Column(name = "STATUS", columnDefinition = "BIT default 0", length = 1)
    private Boolean status;
    @Column(name = "LAST_ACCESS")
    private LocalDateTime lastAccess;
    @Column(name = "RESET_CREDENTIALS_REQ")
    private String resetCredentialsReq;
    @Column(name = "RESET_CREDENTIALS_EXP")
    private LocalDate resetCredentialsExp;
    @OneToOne
    @JoinColumn(name = "GROUP_ID", referencedColumnName = "ID")
    private GroupInfo groupInfo;

}

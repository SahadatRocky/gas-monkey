package com.ba.gas.monkey.repositories;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.models.OrderProduct;

public interface OrderProductRepository extends BaseRepository<OrderProduct> {
}

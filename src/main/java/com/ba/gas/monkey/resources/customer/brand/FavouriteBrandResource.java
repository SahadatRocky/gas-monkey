package com.ba.gas.monkey.resources.customer.brand;

import com.ba.gas.monkey.dtos.FavouriteBrandBean;
import com.ba.gas.monkey.dtos.brand.FavouriteBrandRequestBean;
import com.ba.gas.monkey.dtos.product.ProductDetailFilterBean;
import com.ba.gas.monkey.resources.customer.CustomerV1API;
import com.ba.gas.monkey.services.FavouriteBrandService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;

@CustomerV1API
@AllArgsConstructor
public class FavouriteBrandResource {

    private final FavouriteBrandService service;

    @PostMapping("favourite-brand")
    public ResponseEntity<?> favouriteBrand(@RequestBody @Valid FavouriteBrandRequestBean bean) {
        service.favouriteBrand(bean);
        return new ResponseEntity<>("done", HttpStatus.OK);
    }
}

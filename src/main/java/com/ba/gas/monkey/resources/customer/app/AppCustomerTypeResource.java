package com.ba.gas.monkey.resources.customer.app;

import com.ba.gas.monkey.resources.customer.CustomerV1API;
import com.ba.gas.monkey.services.customer.CustomerTypeService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;

@CustomerV1API
@AllArgsConstructor
public class AppCustomerTypeResource {

    private final CustomerTypeService service;

    @GetMapping("customer-type/dropdown-list")
    public ResponseEntity<?> getCustomerTypeDropdownList() {
        return new ResponseEntity<>(service.getCustomerTypeDropdownList(), HttpStatus.OK);
    }

}

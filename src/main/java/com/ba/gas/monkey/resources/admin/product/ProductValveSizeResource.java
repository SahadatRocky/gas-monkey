package com.ba.gas.monkey.resources.admin.product;

import com.ba.gas.monkey.resources.admin.AdminV1API;
import com.ba.gas.monkey.services.product.ProductValveSizeService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;

@AdminV1API
@AllArgsConstructor
public class ProductValveSizeResource {

    private final ProductValveSizeService service;

    @GetMapping("product-valve-size")
    public ResponseEntity<?> getList(Pageable pageable) {
        return new ResponseEntity<>(service.getList(pageable), HttpStatus.OK);
    }

    @GetMapping("product-valve-size/dropdown-list")
    public ResponseEntity<?> getDropdownList() {
        return new ResponseEntity<>(service.getDropdownList(), HttpStatus.OK);
    }
}

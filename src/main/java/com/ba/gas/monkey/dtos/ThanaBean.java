package com.ba.gas.monkey.dtos;

import lombok.Data;

@Data
public class ThanaBean implements IRequestBodyDTO{
    private String name;
    private Boolean status;
}

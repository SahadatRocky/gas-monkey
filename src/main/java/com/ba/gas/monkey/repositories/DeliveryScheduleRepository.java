package com.ba.gas.monkey.repositories;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.models.DeliverySchedule;

public interface DeliveryScheduleRepository extends BaseRepository<DeliverySchedule>  {

}

package com.ba.gas.monkey.services.refreshtoken;

import com.ba.gas.monkey.dtos.refreshtoken.RefreshTokenBean;

import java.util.Optional;

public interface RefreshTokenService {

    RefreshTokenBean createRefreshToken(String userId);

    Optional<RefreshTokenBean> getByToken(String token);

    RefreshTokenBean verifyExpiration(RefreshTokenBean refreshTokenBean);
}

package com.ba.gas.monkey.models;

import com.ba.gas.monkey.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.time.LocalDate;

@Data
@Entity
@Table(name = "coupon")
@EqualsAndHashCode(callSuper = true)
public class Coupon extends BaseEntity {

    @Column(name = "COUPON_CODE")
    private String couponCode;
    @Column(name = "COUPON_TYPE", columnDefinition = "ENUM")
    private String couponType;
    @Column(name = "COUPON_USAGE_TYPE", columnDefinition = "ENUM")
    private String couponUsageType;
    @Column(name = "AMOUNT")
    private Double amount;
    @Column(name = "USAGE_LIMIT")
    private Integer usageLimit;
    @Column(name = "COUPON_USED")
    private Integer couponUsed;
    @Column(name = "STATUS", columnDefinition = "BIT default 0", length = 1)
    private Boolean status;
    @Column(name = "START_DATE")
    private LocalDate startDate;
    @Column(name = "END_DATE")
    private LocalDate endDate;
    

}

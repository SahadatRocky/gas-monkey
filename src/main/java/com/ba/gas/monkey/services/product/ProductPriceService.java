package com.ba.gas.monkey.services.product;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.base.BaseService;
import com.ba.gas.monkey.dtos.product.ProductPriceBean;
import com.ba.gas.monkey.models.Product;
import com.ba.gas.monkey.models.ProductPrice;
import com.ba.gas.monkey.repositories.ProductPriceRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service("productPriceService")
public class ProductPriceService extends BaseService<ProductPrice, ProductPriceBean> {
    public ProductPriceService(BaseRepository<ProductPrice> repository, ModelMapper modelMapper) {
        super(repository, modelMapper);
    }

    public void updateProductPrice(Product updatedProduct, ProductPriceBean productPriceBean) {
        Optional<ProductPrice> optional = ((ProductPriceRepository) getRepository()).findByProduct(updatedProduct);
        ProductPrice productPrice;
        if (optional.isPresent()) {
            productPrice = optional.get();
            BeanUtils.copyProperties(productPriceBean, productPrice, "id", "dateCreated");
            productPrice.setProduct(updatedProduct);
            update(optional.get().getId(), productPrice);
        } else {
            productPrice = convertForCreate(productPriceBean);
            productPrice.setProduct(updatedProduct);
            create(productPrice);
        }
    }
}

package com.ba.gas.monkey.resources.customer;

import com.ba.gas.monkey.dtos.cart.AddCartBeanRequest;
import com.ba.gas.monkey.dtos.cart.DeleteCartBeanRequest;
import com.ba.gas.monkey.services.cart.CartDetailService;
import com.ba.gas.monkey.services.cart.CartService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@CustomerV1API
@AllArgsConstructor
public class CartResource {

    private final CartService cartService;
    private final CartDetailService cartDetailService;

    @PostMapping("add-to-cart")
    public ResponseEntity<?> addToCart(@RequestBody @Valid AddCartBeanRequest bean) {
        return new ResponseEntity<>(cartService.addToCart(bean), HttpStatus.OK);
    }

    @PostMapping("delete-from-cart")
    public ResponseEntity<?> deleteFromCart(@RequestBody @Valid DeleteCartBeanRequest bean) {
        return new ResponseEntity<>(cartService.deleteFromCart(bean), HttpStatus.OK);
    }

    @GetMapping("get-cart/{id}")
    public ResponseEntity<?> getCart(@PathVariable String id) {
        return new ResponseEntity<>(cartService.getCart(id), HttpStatus.OK);
    }
}

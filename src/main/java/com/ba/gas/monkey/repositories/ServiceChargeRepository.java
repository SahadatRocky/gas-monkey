package com.ba.gas.monkey.repositories;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.models.ServiceCharge;

import java.util.Optional;

public interface ServiceChargeRepository extends BaseRepository<ServiceCharge> {
    Optional<ServiceCharge> findByServiceName(String serviceName);
}

package com.ba.gas.monkey.models;

import com.ba.gas.monkey.base.BaseEntity;
import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;


@Data
@Entity
@Table(name = "brand_image")
@EqualsAndHashCode(callSuper = true)
public class BrandImage extends BaseEntity {

    @Column(name = "TITLE")
    private String title;
    @Column(name = "IMAGE_LINK")
    private String imageLink;
    @Column(name = "IMAGE_LINK_ID")
    private String imageLinkId;
    @Column(name = "STATUS", columnDefinition = "BIT default 0", length = 1)
    private Boolean status;
    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "BRAND_ID", referencedColumnName = "ID")
    private Brand brand;
}

package com.ba.gas.monkey.dtos.customer;


import com.ba.gas.monkey.dtos.IRequestBodyDTO;
import lombok.Data;

@Data
public class CustomerTypeBean implements IRequestBodyDTO {
    private String customerName;
    private String customerType;
}

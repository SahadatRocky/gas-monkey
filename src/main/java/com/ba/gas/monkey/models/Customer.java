package com.ba.gas.monkey.models;

import com.ba.gas.monkey.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

@Data
@Entity
@Table(name = "customer")
@EqualsAndHashCode(callSuper = true)
public class Customer extends BaseEntity {

    @Column(name = "CUSTOMER_NAME")
    private String customerName;
    @Column(name = "EMAIL_ADDRESS")
    private String emailAddress;
    @Column(name = "PHONE_NO")
    private String phoneNo;
    @Column(name = "COMPANY_NAME")
    private String companyName;
    @Column(name = "STATUS")
    private Boolean status;
    @OneToOne
    @JoinColumn(name = "DISTRICT_ID", referencedColumnName = "ID")
    private District district;
    @OneToOne
    @JoinColumn(name = "THANA_ID", referencedColumnName = "ID")
    private Thana thana;
    @OneToOne
    @JoinColumn(name = "CLUSTER_ID", referencedColumnName = "ID")
    private Cluster cluster;
    @Column(name = "AREA")
    private String area;
    @Column(name = "ADDRESS")
    private String address;
    @Column(name = "REWARD_POINT")
    private Integer rewardPoint;
    @Column(name = "PASSWORD")
    private String password;
    @Column(name = "LATITUDE")
    private Double latitude;
    @Column(name = "LONGITUDE")
    private Double longitude;
    @Column(name = "FLOOR_NO")
    private String floorNo;
    @Column(name = "LIFT_ALLOWED")
    private Boolean liftAllowed;
    @OneToOne
    @JoinColumn(name = "TYPE", referencedColumnName = "ID")
    private CustomerType customerType;
}

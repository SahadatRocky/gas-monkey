package com.ba.gas.monkey.models;

import com.ba.gas.monkey.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;


@Data
@Entity
@Table(name = "group_info")
@EqualsAndHashCode(callSuper = true)
public class GroupInfo extends BaseEntity {
    @Column(name = "GROUP_NAME")
    private String groupName;
    @Column(name = "GROUP_TYPE")
    private String groupType;
}

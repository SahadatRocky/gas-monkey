package com.ba.gas.monkey.dtos.order;

import com.ba.gas.monkey.dtos.IRequestBodyDTO;
import lombok.Builder;
import lombok.Data;

import java.time.Instant;

@Data
@Builder
public class OrderInfoListBean implements IRequestBodyDTO {
    private String id;
    private Long orderNumber;
    private String customerName;
    private String partnerName;
    private String dealerName;
    private String customerPhoneNo;
    private String productPhoto;
    private String brandName;
    private Integer orderQuantity;
    private String productSize;
    private Double price;
    private String valveSize;
    private Instant orderDate;
    private String orderStatus;
}

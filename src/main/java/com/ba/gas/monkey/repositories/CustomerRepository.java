package com.ba.gas.monkey.repositories;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.models.Customer;

import java.util.Optional;

public interface CustomerRepository extends BaseRepository<Customer> {
    Optional<Customer> findByPhoneNo(String phoneNo);
}

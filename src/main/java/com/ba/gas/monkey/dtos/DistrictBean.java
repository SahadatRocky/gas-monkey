package com.ba.gas.monkey.dtos;

import lombok.Data;

@Data
public class DistrictBean implements IRequestBodyDTO{
    private String name;
    private Boolean status;
}

package com.ba.gas.monkey.models;

import com.ba.gas.monkey.base.BaseEntity;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;


@Data
@Entity
@Table(name = "order_info")
@EqualsAndHashCode(callSuper = true)
public class OrderInfo extends BaseEntity {
    @JsonManagedReference
    @OneToOne
    @JoinColumn(name = "CUSTOMER_ID", referencedColumnName = "ID")
    private Customer customer;
    @Column(name = "STATUS", columnDefinition = "BIT default 0", length = 1)
    private Boolean status;
    @Column(name = "ORDER_STATUS", columnDefinition = "ENUM")
    private String orderStatus;
    @Column(name = "ORDER_NUMBER")
    private Long orderNumber;
    @Column(name = "SUB_TOTAL")
    private Double subTotal;
    @Column(name = "VAT")
    private Double vat;
    @Column(name = "SERVICE_CHARGE")
    private Double serviceCharge;
    @Column(name = "DISCOUNT_AMOUNT")
    private Double discountAmount;
    @Column(name = "TOTAL")
    private Double total;
    @Column(name = "COUPON_CODE")
    private String couponCode;
    @Column(name = "NOTE", columnDefinition = "TEXT")
    private String note;
    @Column(name = "REGULAR_DELIVERY", columnDefinition = "BIT default 0", length = 1)
    private Boolean regularDelivery;
    @Column(name = "DELIVERY_DATE")
    private LocalDateTime deliveryDate;
    @Column(name = "DELIVERY_SLOT")
    private String deliverySlot;
    @Column(name = "DELIVERY_PAYMENT_TYPE", columnDefinition = "ENUM")
    private String deliveryPaymentType;
    @Column(name = "DELIVERY_MAP_ADDRESS", columnDefinition = "TEXT")
    private String deliveryMapAddress;
    @Column(name = "DELIVERY_LAT")
    private Double deliveryLat;
    @Column(name = "DELIVERY_LONG")
    private Double deliveryLong;
    @OneToOne
    @JoinColumn(name = "DISTRICT_ID", referencedColumnName = "ID")
    private District district;
    @OneToOne
    @JoinColumn(name = "THANA_ID", referencedColumnName = "ID")
    private Thana thana;
    @OneToOne
    @JoinColumn(name = "CLUSTER_ID", referencedColumnName = "ID")
    private Cluster cluster;

    @Column(name = "DELIVERY_AREA", columnDefinition = "TEXT")
    private String deliveryArea;

    @Column(name = "LIFT", columnDefinition = "BIT default 0", length = 1)
    private Boolean lift;

    @Column(name = "FLOOR")
    private String floor;

    @JsonManagedReference
    @OneToMany(mappedBy = "orderInfo", fetch = FetchType.EAGER)
    private List<OrderProduct> orderProductList;

}

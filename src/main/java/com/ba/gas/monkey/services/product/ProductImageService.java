package com.ba.gas.monkey.services.product;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.base.BaseService;
import com.ba.gas.monkey.constants.AppConstant;
import com.ba.gas.monkey.dtos.DropdownDTO;
import com.ba.gas.monkey.dtos.product.ProductImageBean;
import com.ba.gas.monkey.exception.ServiceExceptionHolder;
import com.ba.gas.monkey.models.ProductImage;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service("productImageService")
public class ProductImageService extends BaseService<ProductImage, ProductImageBean> {

    private final ProductService productService;

    public ProductImageService(BaseRepository<ProductImage> repository, ModelMapper modelMapper, ProductService productService) {
        super(repository, modelMapper);
        this.productService = productService;
    }

    public List<DropdownDTO> getDropdownList() {
        List<ProductImage> groupInfos = getRepository().findAll();
        return groupInfos.stream().map(this::getDropdownDto).collect(Collectors.toUnmodifiableList());
    }

    private DropdownDTO getDropdownDto(ProductImage productImage) {
        DropdownDTO dropdownDTO = getModelMapper().map(productImage, DropdownDTO.class);
        dropdownDTO.setValue(productImage.getImageLink());
        return dropdownDTO;
    }

    public ProductImageBean createProductImage(ProductImageBean data) {
        ProductImage productImage= convertForCreate(data);
        productService.getRepository().findById(data.getProductId()).ifPresent(productImage::setProduct);
        return super.create(productImage);
    }

    public String saveFile(MultipartFile file){
        try {
            String destination = UUID.randomUUID() +file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("."));
            System.out.println(destination);
            String filePath = "/gas-monkey-data/" + destination;
            File fileToSave = new File(filePath);
            file.transferTo(fileToSave);
            return "https://uat-gasmonkey.oss.net.bd/uat-gas-monkey-file/"+destination;
        } catch (Exception e) {
            return e.getMessage();
        }
    }

    public String deleteByOid(String id) {
        getRepository().deleteById(id);
        return "delete successful";
    }

    public ProductImageBean updateProductImage(String id, ProductImageBean bean) {
        ProductImage productImage = getRepository().findById(id).orElseThrow(() -> new ServiceExceptionHolder.IdNotFoundInDBException("No brand-image found by id:" + id));
        BeanUtils.copyProperties(bean, productImage, AppConstant.IGNORE_PROPERTIES);
        return update(id, productImage);
    }
}

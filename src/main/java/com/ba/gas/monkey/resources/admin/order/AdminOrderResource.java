package com.ba.gas.monkey.resources.admin.order;

import com.ba.gas.monkey.resources.admin.AdminV1API;
import com.ba.gas.monkey.services.order.OrderProductService;
import com.ba.gas.monkey.services.order.OrderService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@AdminV1API
@AllArgsConstructor
public class AdminOrderResource {

    private final OrderService orderService;
    private final OrderProductService orderProductService;

    @GetMapping("order-list")
    public ResponseEntity<?> getOrderList(Pageable pageable) {
        return new ResponseEntity<>(orderService.getOrderList(pageable), HttpStatus.OK);
    }

    @GetMapping("get-order-by-id/{id}")
    public ResponseEntity<?> getOrderById(@PathVariable String id) {
        return new ResponseEntity<>(orderService.getOrder(id), HttpStatus.OK);
    }

}

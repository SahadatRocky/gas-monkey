package com.ba.gas.monkey.repositories;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.models.CustomerType;

public interface CustomerTypeRepository extends BaseRepository<CustomerType> {
}

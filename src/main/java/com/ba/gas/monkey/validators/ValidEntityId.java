package com.ba.gas.monkey.validators;

import com.ba.gas.monkey.base.BaseEntity;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Constraint(validatedBy = EntityIdValidityCheckerExtended.class)
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidEntityId {

    Class<? extends BaseEntity> value();

    String message() default "invalid id provided";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

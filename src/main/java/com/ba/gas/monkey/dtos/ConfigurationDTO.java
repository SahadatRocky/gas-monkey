package com.ba.gas.monkey.dtos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ConfigurationDTO {

    List<ServiceChargeBean> charges;

    DeliveryScheduleBean scheduleBean;
}

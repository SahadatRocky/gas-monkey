package com.ba.gas.monkey.dtos.refreshtoken;

import com.ba.gas.monkey.models.UserInfo;
import lombok.Data;

import java.time.Instant;

@Data
public class RefreshTokenBean {
    private String id;
    private UserInfo user;
    private String token;
    private Instant expiryDate;
}

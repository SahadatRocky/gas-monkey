package com.ba.gas.monkey.services.cart;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.base.BaseService;
import com.ba.gas.monkey.dtos.cart.AddCartBeanRequest;
import com.ba.gas.monkey.dtos.cart.CartBean;
import com.ba.gas.monkey.dtos.cart.DeleteCartBeanRequest;
import com.ba.gas.monkey.dtos.cart.GetCartResponseBean;
import com.ba.gas.monkey.models.Cart;
import com.ba.gas.monkey.models.CartDetail;
import com.ba.gas.monkey.models.Customer;
import com.ba.gas.monkey.repositories.CartRepository;
import com.ba.gas.monkey.repositories.ServiceChargeRepository;
import com.ba.gas.monkey.services.ServiceChargeService;
import com.ba.gas.monkey.services.customer.CustomerService;
import com.ba.gas.monkey.services.product.ProductService;
import com.ba.gas.monkey.services.user.UserInfoService;
import com.ba.gas.monkey.utility.DateUtils;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Optional;

@Service("cartService")
public class CartService extends BaseService<Cart, CartBean> {

    private final CartRepository cartRepository;
    private final CartDetailService cartDetailService;
    private final UserInfoService userInfoService;
    private final CustomerService customerService;
    private final ProductService productService;
    private final ServiceChargeService serviceChargeService;

    public CartService(BaseRepository<Cart> repository, ModelMapper modelMapper, CartRepository cartRepository,
                       CartDetailService cartDetailService, UserInfoService userInfoService,
                       CustomerService customerService, ProductService productService,
                       ServiceChargeService serviceChargeService) {
        super(repository, modelMapper);
        this.cartRepository = cartRepository;
        this.cartDetailService = cartDetailService;
        this.userInfoService = userInfoService;
        this.customerService = customerService;
        this.productService = productService;
        this.serviceChargeService = serviceChargeService;
    }

    //    @Transactional
    public CartBean addToCart(AddCartBeanRequest addCartBeanRequest) {
        Cart cart = new Cart();
        Optional<Cart> cartOptional = cartRepository.findByCustomerId(addCartBeanRequest.getCustomerId());
        if (cartOptional.isPresent()) {
            cart = cartOptional.get();
        } else {
            cart.setCustomer(customerService.getRepository().getById(addCartBeanRequest.getCustomerId()));
            cart.setStatus(true);
            cart.setUpdtId(userInfoService.getByEmailAddress("sadmin@batworld.com").get().getId());
            cart = cartRepository.save(cart);
        }
        Cart finalCart = cart;
        CartDetail cartDetail = getModelMapper().map(addCartBeanRequest.getCartDetailBeanRequest(), CartDetail.class);
        cartDetail.setBuyProduct(productService.getRepository().findById(addCartBeanRequest.getCartDetailBeanRequest().getBuyProduct()).get());
        if (addCartBeanRequest.getCartDetailBeanRequest().getReturnProduct() != null) {
            cartDetail.setReturnProduct(productService.getRepository().findById(addCartBeanRequest.getCartDetailBeanRequest().getReturnProduct()).get());
        }
        cartDetail.setCart(finalCart);
        cartDetail.setUpdtId(userInfoService.getByEmailAddress("sadmin@batworld.com").get().getId());
        cartDetailService.getRepository().save(cartDetail);
        return getModelMapper().map(cartRepository.getById(cart.getId()), CartBean.class);
    }

    public CartBean deleteFromCart(DeleteCartBeanRequest deleteCartBeanRequest) {
        cartDetailService.getRepository().deleteById(deleteCartBeanRequest.getCartDetailId());
        Cart cart;
        Optional<Cart> cartOptional = cartRepository.findByCustomerId(deleteCartBeanRequest.getCustomerId());
        if (cartOptional.isPresent()) {
            cart = cartOptional.get();
            return getModelMapper().map(cart, CartBean.class);
        }
        return null;
    }

    public GetCartResponseBean getCart(String id) {
        Customer customer = customerService.getRepository().findById(id).get();
        Optional<Cart> cart = cartRepository.findByCustomerId(customer.getId());
        return getCartResponseBean(cart.get());
    }

    public GetCartResponseBean getCartResponseBean(Cart cart) {
        GetCartResponseBean responseBean = new GetCartResponseBean();
        cart.getCartDetailList().forEach(cartDetail -> {
            Double calculateBuyPrice = cartDetail.getReturnProduct() != null ? cartDetail.getBuyProduct().getProductPrice().getRefillPrice() : cartDetail.getBuyProduct().getProductPrice().getPackagePrice();
            calculateBuyPrice = calculateBuyPrice * cartDetail.getQuantity();
            responseBean.setSubTotal(responseBean.getSubTotal() + calculateBuyPrice);
            responseBean.setDiscountAmount(responseBean.getDiscountAmount()+getDiscountAmount(cartDetail));
            if (cartDetail.getReturnProduct() != null) {
                if (cartDetail.getBuyProduct().getProductPrice().getEmptyCylinderPrice() >= cartDetail.getReturnProduct().getProductPrice().getEmptyCylinderPrice()) {
                    Double exchange = cartDetail.getBuyProduct().getProductPrice().getEmptyCylinderPrice() - cartDetail.getReturnProduct().getProductPrice().getEmptyCylinderPrice();
                    responseBean.setSubTotal(responseBean.getSubTotal() + exchange);
                }
            }
        });

        Double serviceCharge = ((ServiceChargeRepository) serviceChargeService.getRepository()).findByServiceName("SERVICE_CHARGE").get().getServiceValue();
        responseBean.setServiceCharge(serviceCharge*cart.getCartDetailList().size());
        responseBean.setVat(calculateVat(responseBean));
        responseBean.setTotal(calculateTotal(responseBean));
        responseBean.setStatus(cart.getStatus());
        responseBean.setCustomer(cart.getCustomer());
        responseBean.setId(cart.getId());
        responseBean.setCartDetailList(cart.getCartDetailList());
        Double vatPercentage = ((ServiceChargeRepository) serviceChargeService.getRepository()).findByServiceName("VAT_RATE").get().getServiceValue();
        responseBean.setVatPercent(vatPercentage);
        return responseBean;
    }

    public double calculatePrice(CartDetail cartDetail){
        return cartDetail.getReturnProduct() != null?cartDetail.getBuyProduct().getProductPrice().getRefillPrice():cartDetail.getBuyProduct().getProductPrice().getPackagePrice();
    }

    public double getDiscountAmount(CartDetail cartDetail) {
        if(cartDetail.getBuyProduct().getBrand().getDiscountEnabled() && DateUtils.isDateInBetweenIncludingEndPoints(cartDetail.getBuyProduct().getBrand().getDiscountStartDate(), cartDetail.getBuyProduct().getBrand().getDiscountEndDate(),LocalDateTime.now())){
            if(cartDetail.getBuyProduct().getBrand().getDiscountType().equalsIgnoreCase("PERCENT")){
                double buyPrice = calculatePrice(cartDetail);
                return (cartDetail.getBuyProduct().getBrand().getDiscountValue()*buyPrice)/100;
            }else {
                return cartDetail.getBuyProduct().getBrand().getDiscountValue();
            }
        }
        return 0.0;
    }

    public double calculateVat(GetCartResponseBean responseBean) {
        Double vatPercentage = ((ServiceChargeRepository) serviceChargeService.getRepository()).findByServiceName("VAT_RATE").get().getServiceValue();
        double totalAmount = responseBean.getSubTotal() + responseBean.getDiscountAmount();
        return (vatPercentage * totalAmount) / 100;
    }

    public double calculateTotal(GetCartResponseBean responseBean) {
        return responseBean.getSubTotal() - responseBean.getDiscountAmount() + responseBean.getServiceCharge();
    }
}

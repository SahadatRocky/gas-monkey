package com.ba.gas.monkey.paylod.response;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class RefreshTokenResponseBean {
    private String accessToken;
    private String refreshToken;
    private String tokenType;
}

package com.ba.gas.monkey.services;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.base.BaseService;
import com.ba.gas.monkey.constants.AppConstant;
import com.ba.gas.monkey.dtos.DeliveryScheduleBean;
import com.ba.gas.monkey.dtos.FavouriteBrandBean;
import com.ba.gas.monkey.models.DeliverySchedule;
import com.ba.gas.monkey.models.FavouriteBrand;
import com.ba.gas.monkey.models.ServiceCharge;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

@Service("deliveryScheduleService")
public class DeliveryScheduleService extends BaseService<DeliverySchedule, DeliveryScheduleBean> {

    public DeliveryScheduleService(BaseRepository<DeliverySchedule> repository, ModelMapper modelMapper) {
        super(repository, modelMapper);
    }

    public DeliveryScheduleBean getData() {
        return convertForRead(getRepository().findAll().get(0));
    }

    public void updateData(DeliveryScheduleBean scheduleBean) {
        DeliverySchedule deliverySchedule = getRepository().getById(scheduleBean.getId());
        BeanUtils.copyProperties(deliverySchedule, scheduleBean, AppConstant.IGNORE_PROPERTIES);
        deliverySchedule.setId(scheduleBean.getId());
        getRepository().save(deliverySchedule);
    }
}

package com.ba.gas.monkey.dtos.cart;

import com.ba.gas.monkey.dtos.IRequestBodyDTO;
import lombok.Data;

import java.util.List;

@Data
public class DeleteCartBeanRequest implements IRequestBodyDTO {
    private String customerId;
    private String cartDetailId;
}

package com.ba.gas.monkey.services;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.base.BaseService;
import com.ba.gas.monkey.dtos.ServiceDiscountBean;
import com.ba.gas.monkey.models.ServiceDiscount;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service("serviceDiscountService")
public class ServiceDiscountService extends BaseService<ServiceDiscount, ServiceDiscountBean> {

    public ServiceDiscountService(BaseRepository<ServiceDiscount> repository, ModelMapper modelMapper) {
        super(repository, modelMapper);
    }
}

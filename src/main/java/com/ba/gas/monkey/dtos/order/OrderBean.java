package com.ba.gas.monkey.dtos.order;

import com.ba.gas.monkey.dtos.IRequestBodyDTO;
import com.ba.gas.monkey.models.*;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
public class OrderBean implements IRequestBodyDTO {
    private String id;
    private Customer customer;
    private Boolean status;
    private String orderStatus;
    private Long orderNumber;
    private Double subTotal;
    private Double vat;
    private Double serviceCharge;
    private Double discountAmount;
    private Double total;
    private String couponCode;
    private String note;
    private Boolean regularDelivery;
    private LocalDateTime deliveryDate;
    private String deliverySlot;
    private String deliveryPaymentType;
    private String deliveryMapAddress;
    private Double deliveryLat;
    private Double deliveryLong;
    private District district;
    private Thana thana;
    private Cluster cluster;
    private String deliveryArea;
    private Boolean lift;
    private String floor;
    private List<OrderProduct> orderProductList;
}

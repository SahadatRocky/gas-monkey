package com.ba.gas.monkey.repositories;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.models.Coupon;

import java.util.Optional;

public interface CouponRepository extends BaseRepository<Coupon>  {
    Optional<Coupon> findByCouponCode(String couponCode);
}

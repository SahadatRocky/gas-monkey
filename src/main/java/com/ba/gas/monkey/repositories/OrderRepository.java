package com.ba.gas.monkey.repositories;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.models.OrderInfo;

import java.util.List;
import java.util.Optional;

public interface OrderRepository extends BaseRepository<OrderInfo> {
    Optional<List<OrderInfo>> findByCustomerId(String id);
}

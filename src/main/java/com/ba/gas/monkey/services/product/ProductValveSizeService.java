package com.ba.gas.monkey.services.product;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.base.BaseService;
import com.ba.gas.monkey.dtos.DropdownDTO;
import com.ba.gas.monkey.dtos.product.ProductValveSizeBean;
import com.ba.gas.monkey.models.ProductValveSize;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service("productValveSizeService")
public class ProductValveSizeService extends BaseService<ProductValveSize, ProductValveSizeBean> {
    public ProductValveSizeService(BaseRepository<ProductValveSize> repository, ModelMapper modelMapper) {
        super(repository, modelMapper);
    }

    public List<DropdownDTO> getDropdownList() {
        List<ProductValveSize> list = getRepository().findAll(Sort.by("nameEn"));
        return list.stream().map(this::getDropdownDto).collect(Collectors.toUnmodifiableList());
    }

    private DropdownDTO getDropdownDto(ProductValveSize valveSize) {
        DropdownDTO dropdownDTO = getModelMapper().map(valveSize, DropdownDTO.class);
        dropdownDTO.setValue(valveSize.getNameEn() + " (" + valveSize.getNameBn() + ")");
        return dropdownDTO;
    }
}

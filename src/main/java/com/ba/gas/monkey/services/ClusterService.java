package com.ba.gas.monkey.services;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.base.BaseService;
import com.ba.gas.monkey.dtos.ClusterBean;
import com.ba.gas.monkey.dtos.DropdownDTO;
import com.ba.gas.monkey.exception.ServiceExceptionHolder;
import com.ba.gas.monkey.models.Cluster;
import com.ba.gas.monkey.models.Thana;
import com.ba.gas.monkey.repositories.ClusterRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service("clusterService")
public class ClusterService extends BaseService<Cluster, ClusterBean> {

    private final ThanaService thanaService;

    public ClusterService(BaseRepository<Cluster> repository, ModelMapper modelMapper, ThanaService thanaService) {
        super(repository, modelMapper);
        this.thanaService = thanaService;
    }

    public List<DropdownDTO> getDropdownList(String thanaId) {
        Thana thana = thanaService.getRepository().findById(thanaId).orElseThrow(() -> new ServiceExceptionHolder.IdNotFoundInDBException("No thana found by id :" + thanaId));
        List<Cluster> clusters = ((ClusterRepository) getRepository()).findByThana(thana);
        return clusters.stream().map(this::getDropdownDTO).collect(Collectors.toUnmodifiableList());
    }

    private DropdownDTO getDropdownDTO(Cluster cluster) {
        DropdownDTO dto = getModelMapper().map(cluster, DropdownDTO.class);
        dto.setValue(cluster.getName());
        return dto;
    }

}

package com.ba.gas.monkey.repositories;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.models.BrandImage;

public interface BrandImageRepository extends BaseRepository<BrandImage>  {

}

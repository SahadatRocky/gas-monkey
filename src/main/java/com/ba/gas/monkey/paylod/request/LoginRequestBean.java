package com.ba.gas.monkey.paylod.request;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class LoginRequestBean {
    @NotBlank
    private String username;
    @NotBlank
    private String password;
}

package com.ba.gas.monkey.repositories;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.models.ProductSize;

public interface ProductSizeRepository extends BaseRepository<ProductSize> {
}

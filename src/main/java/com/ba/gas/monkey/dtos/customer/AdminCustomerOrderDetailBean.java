package com.ba.gas.monkey.dtos.customer;


import com.ba.gas.monkey.dtos.IRequestBodyDTO;
import lombok.Data;

@Data
public class AdminCustomerOrderDetailBean implements IRequestBodyDTO {
    private String id;
    private String customerName;
    private String phoneNo;
    private String address;
}

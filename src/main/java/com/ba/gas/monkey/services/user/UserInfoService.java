package com.ba.gas.monkey.services.user;

import com.ba.gas.monkey.dtos.user.UserInfoBean;
import com.ba.gas.monkey.dtos.user.UserInfoEditBean;
import com.ba.gas.monkey.dtos.user.UserListBean;
import com.ba.gas.monkey.models.UserInfo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

public interface UserInfoService {

    Page<UserListBean> getList(Pageable pageable);

    Optional<UserInfoBean> getByEmailAddress(String emailAddress);

    Optional<UserInfo> getById(String userId);

    UserInfoBean getByUserId(String userId);

    UserInfoBean getUserDetails(UserInfo userInfo);

    Optional<UserInfoBean> updateUser(String id, UserInfoEditBean userInfoBean);

    void update(UserInfo userInfo);
}

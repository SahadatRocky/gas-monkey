package com.ba.gas.monkey.repositories;


import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.models.CustomerType;
import com.ba.gas.monkey.models.Department;

public interface DepartmentRepository extends BaseRepository<Department> {

}

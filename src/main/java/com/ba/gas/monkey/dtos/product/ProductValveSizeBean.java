package com.ba.gas.monkey.dtos.product;

import com.ba.gas.monkey.dtos.IRequestBodyDTO;
import lombok.Data;

@Data
public class ProductValveSizeBean implements IRequestBodyDTO {
    private String id;
    private String nameEn;
    private String nameBn;
    private Boolean status;
}

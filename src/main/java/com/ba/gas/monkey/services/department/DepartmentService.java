package com.ba.gas.monkey.services.department;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.base.BaseService;
import com.ba.gas.monkey.dtos.DropdownDTO;
import com.ba.gas.monkey.dtos.department.DepartmentBean;
import com.ba.gas.monkey.models.Department;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;


@Service("departmentService")
public class DepartmentService extends BaseService<Department, DepartmentBean> {

    public DepartmentService(BaseRepository<Department> repository, ModelMapper modelMapper) {
        super(repository, modelMapper);
    }

    public List<DropdownDTO> getDropdownList() {
        List<Department> list = getRepository().findAll(Sort.by("name"));
        return list.stream().map(this::getDropdownDto).collect(Collectors.toUnmodifiableList());
    }

    private DropdownDTO getDropdownDto(Department department) {
        DropdownDTO dropdownDTO = getModelMapper().map(department, DropdownDTO.class);
        dropdownDTO.setValue(department.getName());
        return dropdownDTO;
    }

}

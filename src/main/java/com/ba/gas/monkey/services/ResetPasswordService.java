package com.ba.gas.monkey.services;

import com.ba.gas.monkey.dtos.ResetPasswordBean;
import com.ba.gas.monkey.dtos.user.UserInfoBean;
import com.ba.gas.monkey.exception.ServiceExceptionHolder;
import com.ba.gas.monkey.models.UserInfo;
import com.ba.gas.monkey.services.user.UserInfoService;
import lombok.AllArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@AllArgsConstructor
@Service("resetPasswordService")
public class ResetPasswordService {

    private final UserInfoService userInfoService;
    private final PasswordEncoder passwordEncoder;

    public void resetPassword(ResetPasswordBean bean) {
        String password = bean.getPassword();
        String confirmPassword = bean.getConfirmPassword();
        if (!password.equals(confirmPassword)) {
            throw new ServiceExceptionHolder.CustomException(ServiceExceptionHolder.PASSWORD_MISMATCH_EXCEPTION_CODE, "please check password");
        }
        UserInfo userInfo = userInfoService.getById(bean.getUserId()).orElseThrow(() -> new ServiceExceptionHolder.IdNotFoundInDBException("Id not found"));
        userInfo.setPassword(passwordEncoder.encode(password));
        userInfoService.update(userInfo);
    }
}

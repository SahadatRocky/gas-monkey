FROM openjdk:11-slim-buster
RUN ls 
ADD target/gas-monkey-1.0.jar gas-monkey-1.0.jar
EXPOSE 8801
ENTRYPOINT ["java","-jar","gas-monkey-1.0.jar"]

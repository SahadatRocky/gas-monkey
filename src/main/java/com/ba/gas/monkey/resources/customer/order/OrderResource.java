package com.ba.gas.monkey.resources.customer.order;

import com.ba.gas.monkey.dtos.cart.AddCartBeanRequest;
import com.ba.gas.monkey.dtos.cart.DeleteCartBeanRequest;
import com.ba.gas.monkey.dtos.order.CreateOrderRequestBean;
import com.ba.gas.monkey.resources.customer.CustomerV1API;
import com.ba.gas.monkey.services.cart.CartDetailService;
import com.ba.gas.monkey.services.cart.CartService;
import com.ba.gas.monkey.services.order.OrderProductService;
import com.ba.gas.monkey.services.order.OrderService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;

@CustomerV1API
@AllArgsConstructor
public class OrderResource {

    private final OrderService orderService;
    private final OrderProductService orderProductService;

    @PostMapping("order")
    public ResponseEntity<?> createOrder(@RequestBody @Valid CreateOrderRequestBean bean) {
        return new ResponseEntity<>(orderService.createOrder(bean), HttpStatus.OK);
    }

    @GetMapping("order-list/{id}")
    public ResponseEntity<?> orderList(@PathVariable String id) {
        return new ResponseEntity<>(orderService.getOrderList(id), HttpStatus.OK);
    }

    @GetMapping("get-order/{id}")
    public ResponseEntity<?> getOrder(@PathVariable String id) {
        return new ResponseEntity<>(orderService.getOrder(id), HttpStatus.OK);
    }
}

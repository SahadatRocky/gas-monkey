package com.ba.gas.monkey.services.product;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.base.BaseService;
import com.ba.gas.monkey.dtos.DropdownDTO;
import com.ba.gas.monkey.dtos.product.ProductSizeBean;
import com.ba.gas.monkey.models.ProductSize;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service("productSizeService")
public class ProductSizeService extends BaseService<ProductSize, ProductSizeBean> {
    public ProductSizeService(BaseRepository<ProductSize> repository, ModelMapper modelMapper) {
        super(repository, modelMapper);
    }

    public List<DropdownDTO> getDropdownList() {
        List<ProductSize> list = getRepository().findAll(Sort.by("nameEn"));
        return list.stream().map(this::getDropdownDto).collect(Collectors.toUnmodifiableList());
    }

    private DropdownDTO getDropdownDto(ProductSize Size) {
        DropdownDTO dropdownDTO = getModelMapper().map(Size, DropdownDTO.class);
        dropdownDTO.setValue(Size.getNameEn() + " (" + Size.getNameBn() + ")");
        return dropdownDTO;
    }
}

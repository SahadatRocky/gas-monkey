package com.ba.gas.monkey.services.cart;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.base.BaseService;
import com.ba.gas.monkey.dtos.DropdownDTO;
import com.ba.gas.monkey.dtos.cart.CartDetailBean;
import com.ba.gas.monkey.dtos.cart.CartDetailBeanRequest;
import com.ba.gas.monkey.dtos.cart.CartDetailsResponseBean;
import com.ba.gas.monkey.dtos.product.ProductImageBean;
import com.ba.gas.monkey.models.Cart;
import com.ba.gas.monkey.models.CartDetail;
import com.ba.gas.monkey.models.ProductImage;
import com.ba.gas.monkey.repositories.CartDetailRepository;
import com.ba.gas.monkey.services.product.ProductService;
import com.ba.gas.monkey.services.user.UserInfoService;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;

@Service("cartDetailService")
public class CartDetailService extends BaseService<CartDetail, CartDetailBean> {
    public CartDetailService(BaseRepository<CartDetail> repository, ModelMapper modelMapper) {
        super(repository, modelMapper);
    }
}

package com.ba.gas.monkey.services.customer;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.base.BaseService;
import com.ba.gas.monkey.constants.AppConstant;
import com.ba.gas.monkey.dtos.customer.CustomerBean;
import com.ba.gas.monkey.dtos.customer.CustomerInfoBean;
import com.ba.gas.monkey.dtos.customer.CustomerListBean;
import com.ba.gas.monkey.dtos.customer.CustomerRegistrationBean;
import com.ba.gas.monkey.dtos.user.LoggedInUserBean;
import com.ba.gas.monkey.exception.ServiceExceptionHolder;
import com.ba.gas.monkey.models.Customer;
import com.ba.gas.monkey.repositories.CustomerRepository;
import com.ba.gas.monkey.services.ClusterService;
import com.ba.gas.monkey.services.DistrictService;
import com.ba.gas.monkey.services.ThanaService;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@Service("customerService")
public class CustomerService extends BaseService<Customer, CustomerBean> {

    private final CustomerTypeService customerTypeService;
    private final DistrictService districtService;
    private final ThanaService thanaService;
    private final ClusterService clusterService;

    public CustomerService(BaseRepository<Customer> repository, ModelMapper modelMapper,
                           CustomerTypeService customerTypeService,
                           DistrictService districtService,
                           ThanaService thanaService,
                           ClusterService clusterService) {
        super(repository, modelMapper);
        this.customerTypeService = customerTypeService;
        this.districtService = districtService;
        this.thanaService = thanaService;
        this.clusterService = clusterService;
    }

    public Page<CustomerListBean> getCustomerListBeans(Pageable pageable) {
        PageRequest pageRequest = PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), Sort.by(Sort.Direction.DESC, "dateCreated"));
        Page<Customer> page = getRepository().findAll(pageRequest);
        List<CustomerListBean> list = page.getContent().stream().map(this::getListBean).collect(Collectors.toUnmodifiableList());
        AtomicInteger increment = new AtomicInteger(1);
        list.forEach(l -> l.setSlNo(increment.getAndIncrement()));
        return new PageImpl<>(list, page.getPageable(), page.getTotalElements());
    }

    public CustomerBean createCustomer(CustomerBean customerBean) {
        Customer customer = convertForCreate(customerBean);
        customer.setDistrict(districtService.getRepository().getById(customerBean.getDistrictId()));
        customer.setThana(thanaService.getRepository().getById(customerBean.getThanaId()));
        customer.setCluster(clusterService.getRepository().getById(customerBean.getClusterId()));
        customer.setCustomerType(customerTypeService.getRepository().getById(customerBean.getCustomerTypeId()));
        return create(customer);
    }

    public Optional<CustomerBean> getByPhoneNo(String phoneNo) {
        Optional<Customer> optionalCustomer = ((CustomerRepository) getRepository()).findByPhoneNo(phoneNo);
        if (optionalCustomer.isEmpty()) {
            return Optional.empty();
        }
        CustomerBean customerBean = getModelMapper().map(optionalCustomer.get(), CustomerBean.class);
        List<String> authorities = Collections.singletonList(optionalCustomer.get().getCustomerType().getCustomerType());
        customerBean.setAuthorities(authorities);
        return Optional.of(customerBean);
    }


    public LoggedInUserBean getCustomerByPhoneNo(String phoneNo) {
        Customer customer = ((CustomerRepository) getRepository()).findByPhoneNo(phoneNo).orElseThrow(() -> new ServiceExceptionHolder.IdNotFoundInDBException("No user found by:" + phoneNo));
        List<String> authorities = Collections.singletonList(customer.getCustomerType().getCustomerType());
        return LoggedInUserBean.builder()
                .id(customer.getId())
                .authorities(authorities)
                .email(customer.getPhoneNo())
                .password(customer.getPassword())
                .status(customer.getStatus())
                .build();
    }

    private CustomerListBean getListBean(Customer customer) {
        CustomerListBean bean = getModelMapper().map(customer, CustomerListBean.class);
        bean.setStatus(customer.getStatus() ? AppConstant.ACTIVE : AppConstant.INACTIVE);
        bean.setCustomerType(customer.getCustomerType().getCustomerName());
        return bean;
    }

    public CustomerInfoBean registration(CustomerRegistrationBean bean) {
        Customer customer = getModelMapper().map(bean, Customer.class);
        customer.setCustomerName(bean.getCustomerName());
        customer.setStatus(AppConstant.ACTIVE_USER);
        customer.setDateCreated(Instant.now());
        customer.setDateModified(Instant.now());
        customer.setUpdtId(bean.getUpdtId());
        customer.setDistrict(districtService.getRepository().getById(bean.getDistrictId()));
        customer.setThana(thanaService.getRepository().getById(bean.getThanaId()));
        customer.setCluster(clusterService.getRepository().getById(bean.getClusterId()));
        customer.setCustomerType(customerTypeService.getRepository().getById(bean.getCustomerTypeId()));
        Customer createdCustomer = getRepository().save(customer);
        return getModelMapper().map(createdCustomer, CustomerInfoBean.class);
    }
}

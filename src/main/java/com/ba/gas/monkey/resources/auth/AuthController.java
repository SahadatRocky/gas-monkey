package com.ba.gas.monkey.resources.auth;

import com.ba.gas.monkey.constants.AppConstant;
import com.ba.gas.monkey.paylod.response.JwtTokenResponseBean;
import com.ba.gas.monkey.paylod.request.LoginRequestBean;
import com.ba.gas.monkey.paylod.response.RefreshTokenResponseBean;
import com.ba.gas.monkey.paylod.request.TokenRefreshRequestBean;
import com.ba.gas.monkey.dtos.refreshtoken.RefreshTokenBean;
import com.ba.gas.monkey.exception.ServiceExceptionHolder;
import com.ba.gas.monkey.security.admin.UserDetailsImpl;
import com.ba.gas.monkey.services.refreshtoken.RefreshTokenService;
import com.ba.gas.monkey.services.user.UserInfoService;
import com.ba.gas.monkey.utility.JwtUtils;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;

@AuthV1APIResource
@AllArgsConstructor
public class AuthController {

    private final JwtUtils jwtUtils;
    private final AuthenticationManager authenticationManager;
    private final RefreshTokenService refreshTokenService;
    private final UserInfoService userInfoService;

    @PostMapping("login")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequestBean loginRequestBean) {
        String username = "ADMIN:" + loginRequestBean.getUsername();
        Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, loginRequestBean.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtUtils.generateJwtToken(authentication);
        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        RefreshTokenBean refreshToken = refreshTokenService.createRefreshToken(userDetails.getUserInfo().getId());
        return ResponseEntity.ok(JwtTokenResponseBean.builder().token(jwt).type(AppConstant.TOKEN_TYPE).refreshToken(refreshToken.getToken()).build());
    }
    /*https://www.bezkoder.com/spring-boot-refresh-token-jwt/*/

    @PostMapping("/refresh-token")
    public ResponseEntity<?> refreshToken(@Valid @RequestBody TokenRefreshRequestBean request) {
        String requestRefreshToken = request.getRefreshToken();
        return refreshTokenService.getByToken(requestRefreshToken)
                .map(refreshTokenService::verifyExpiration)
                .map(RefreshTokenBean::getUser)
                .map(userInfoService::getUserDetails)
                .map(userInfoDTO -> {
                    String token = jwtUtils.generateTokenFromUsername(userInfoDTO);
                    return ResponseEntity.ok(RefreshTokenResponseBean.builder().accessToken(token).refreshToken(requestRefreshToken).tokenType(AppConstant.TOKEN_TYPE).build());
                }).orElseThrow(() -> new ServiceExceptionHolder.RefreshTokenException("Refresh token is not in database!"));
    }

    @PostMapping("app/login")
    public ResponseEntity<?> appLogin(@Valid @RequestBody LoginRequestBean loginRequestBean) {
        String username = "APP:" + loginRequestBean.getUsername();
        Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, loginRequestBean.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtUtils.generateJwtToken(authentication);
        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        RefreshTokenBean refreshToken = refreshTokenService.createRefreshToken(userDetails.getUserInfo().getId());
        return ResponseEntity.ok(JwtTokenResponseBean.builder().token(jwt).type(AppConstant.TOKEN_TYPE).refreshToken(refreshToken.getToken()).build());
    }


}

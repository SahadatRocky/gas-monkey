package com.ba.gas.monkey.constants;

import java.util.List;

public class AppConstant {

    public static final String TOKEN_TYPE = "Bearer";
    public static final long JWT_TOKEN_EXPIRE_IN = 1800000;
    public static final long REFRESH_TOKEN_EXPIRE_IN = 86400000;
    public static final String JWT_TOKEN_SECRET = "ind40ch3rn0bY1T3am";
    public static final Boolean ACTIVE_USER = Boolean.TRUE;
    public static final String ACTIVE = "Active";
    public static final String INACTIVE = "Inactive";
    public static final String JWT_TOKEN_ISSUER = "gas-monkey";
    public static final String JWT_TOKEN_SUBJECT = "JWT Token";
    public static final String ROLE_HEADER = "ROLE_";
    public static final String CUSTOMER_TYPE = "CUSTOMER";
    public static final String SUPER_ADMIN_EMAIL = "sadmin@batworld.com";
    public static final String[] IGNORE_PROPERTIES = {"id", "dateCreated"};
    public static final String USER_TYPE_ADMIN = "ADMIN";
    public static final List<String> APP_USER_LIST = List.of("CUSTOMER", "PARTNER", "DEALER");
}

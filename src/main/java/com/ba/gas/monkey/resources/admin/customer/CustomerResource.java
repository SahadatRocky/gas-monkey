package com.ba.gas.monkey.resources.admin.customer;

import com.ba.gas.monkey.dtos.customer.CustomerBean;
import com.ba.gas.monkey.resources.admin.AdminV1API;
import com.ba.gas.monkey.services.customer.CustomerService;
import com.ba.gas.monkey.services.customer.CustomerTypeService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;

@AdminV1API
@AllArgsConstructor
public class CustomerResource {

    private final CustomerService service;

    @GetMapping("customer")
    public ResponseEntity<?> getList(Pageable pageable) {
        return new ResponseEntity<>(service.getCustomerListBeans(pageable), HttpStatus.OK);
    }

    @GetMapping("customer/{id}")
    public ResponseEntity<?> getList(@PathVariable String id) {
        return new ResponseEntity<>(service.getByOid(id), HttpStatus.OK);
    }

    @PostMapping("customer")
    public ResponseEntity<?> createCustomer(@RequestBody @Valid CustomerBean customerBean) {
        return new ResponseEntity<>(service.createCustomer(customerBean), HttpStatus.OK);
    }


}
